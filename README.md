# Safe Villages

This Minecraft data pack overrides spawn rules for all villages to
prevent hostile monsters from spawning within their generated areas.
It is still possible for monsters to spawn at the borders of villages
and wander in.

The current build should be compatible with Minecraft Java edition
1.20.2 – 1.21.4 (data pack formats 18–61).

## Usage

**When creating your world,** go to the “More” tab, click on “Data
Packs”, and “Open Pack Folder”.  Copy `SafeVillages-`_version_`.zip`
into this folder, then it will appear in the list of “Available” data
packs.  Click the rightward arrow on the data pack icon to select it
for your world, ensuring it appears _above_ the “Default” Minecraft
data.

It’s also possible to add this data pack to an existing world.
However, doing so will only affect villages generated _after_ the data
pack has been added, not existing villages.
